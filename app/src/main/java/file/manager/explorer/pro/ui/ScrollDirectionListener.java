package file.manager.explorer.pro.ui;

public interface ScrollDirectionListener {
    void onScrollDown();
    void onScrollUp();
}