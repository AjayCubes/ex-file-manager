package file.manager.explorer.pro.model;

import android.content.ContentProviderClient;
import android.database.Cursor;

import java.io.Closeable;

import file.manager.explorer.pro.libcore.io.IoUtils;
import file.manager.explorer.pro.misc.ContentProviderClientCompat;

import static file.manager.explorer.pro.BaseActivity.State.MODE_UNKNOWN;
import static file.manager.explorer.pro.BaseActivity.State.SORT_ORDER_UNKNOWN;

public class DirectoryResult implements Closeable {
	public ContentProviderClient client;
    public Cursor cursor;
    public Exception exception;

    public int mode = MODE_UNKNOWN;
    public int sortOrder = SORT_ORDER_UNKNOWN;

    @Override
    public void close() {
        IoUtils.closeQuietly(cursor);
        ContentProviderClientCompat.releaseQuietly(client);
        cursor = null;
        client = null;
    }
}