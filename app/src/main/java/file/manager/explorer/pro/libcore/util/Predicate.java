package file.manager.explorer.pro.libcore.util;

public interface Predicate<T> {

    boolean apply(T t);
}