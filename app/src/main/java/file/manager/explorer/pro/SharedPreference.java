package file.manager.explorer.pro;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {
    private static SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    public static void CreatePreferences(Context context, String PreferenceName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static void SavePreferences(String preferenceKey, String preferenceVal) {
        editor.putString(preferenceKey, preferenceVal);
        editor.commit();
    }

    public static String GetPreferences(Context context, String preferenceName, String preferenceKey) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceKey, null);
    }

    public static void ClearPreferences(Context context, String preferenceName) {
        SharedPreferences preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }

    public static void RemovePreferences(Context context, String preferenceName, String preferenceKey) {
        SharedPreferences settings = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        settings.edit().remove(preferenceKey).apply();
    }
}
