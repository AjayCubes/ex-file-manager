package file.manager.explorer.pro.ui;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;

import file.manager.explorer.pro.R;
import file.manager.explorer.pro.misc.CrashReportingManager;

import static file.manager.explorer.pro.DocumentsApplication.isTelevision;


/**
 * A Wrapper which wraps AdView along with loading the view aswell
 */
public class AdWrapper extends FrameLayout {
    private AdView adView;
    private LinearLayout llAdContainer;

    public AdWrapper(Context context) {
        super(context);
        init(context);
    }

    public AdWrapper(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AdWrapper(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        //Ads
        if(!isTelevision()){
            LayoutInflater.from(context).inflate(R.layout.ads_wrapper, this, true);
            initAd(context);
        }
    }

    public void initAd(Context context){
        adView = new AdView(context, context.getResources().getString(R.string.FB_banner_ad_id), AdSize.BANNER_HEIGHT_50);
        llAdContainer = findViewById(R.id.banner_container);
        llAdContainer.addView(adView);
        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                llAdContainer.setVisibility(View.GONE);
            }

            @Override
            public void onAdLoaded(Ad ad) {
                llAdContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
//        showInterstitial();
        if (adView != null) {
            adView.destroy();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        showAd();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        return super.onSaveInstanceState();
    }

    private void showAd(){
//        if(AppPaymentFlavour.isPurchased()){
//            return;
//        }
        if(isInEditMode()){
            return;
        }
        //Fixes GPS AIOB Exception
        try {
            if(null != adView){
                adView.loadAd();
            }
        } catch (Exception e){
            CrashReportingManager.logException(e);
        }
    }
}
