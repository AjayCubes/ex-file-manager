package file.manager.explorer.pro.adapter;

import android.app.Activity;
import android.database.Cursor;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;
import antonkozyriatskyi.circularprogressindicator.PatternProgressTextAdapter;
import file.manager.explorer.pro.R;
import file.manager.explorer.pro.misc.CrashReportingManager;
import file.manager.explorer.pro.misc.IconHelper;
import file.manager.explorer.pro.misc.IconUtils;
import file.manager.explorer.pro.misc.Utils;
import file.manager.explorer.pro.model.DocumentInfo;
import file.manager.explorer.pro.model.RootInfo;
import file.manager.explorer.pro.setting.SettingsActivity;
import file.manager.explorer.pro.ui.CircleImage;
import file.manager.explorer.pro.ui.NumberProgressBar;
import file.manager.explorer.pro.ui.RecyclerViewPlus;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
    public static final int TYPE_MAIN = 1;
    public static final int TYPE_SHORTCUT = 2;
    public static final int TYPE_RECENT = 3;

    private boolean isSDCard = false;

    private final int mDefaultColor;
    private Activity mContext;
    private OnItemClickListener onItemClickListener;
    private ArrayList<CommonInfo> mData = new ArrayList<>();
    private Cursor recentCursor;
    private final IconHelper mIconHelper;

    public HomeAdapter(Activity context, ArrayList<CommonInfo> data, IconHelper iconHelper) {
        mContext = context;
        mData = data;
        mDefaultColor = SettingsActivity.getPrimaryColor();
        mIconHelper = iconHelper;
    }

    public void setData(ArrayList<CommonInfo> data) {
        if (data == mData) {
            return;
        }

        mData = data;
        notifyDataSetChanged();
    }

    public void setRecentData(Cursor cursor) {
        recentCursor = cursor;
        notifyDataSetChanged();
    }

    public int getRecentSize() {
        return recentCursor != null && recentCursor.getCount() != 0 ? 1 : 0;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return mData.size() + getRecentSize();
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_MAIN: {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, parent, false);
                return new MainViewHolder(itemView);
            }
            case TYPE_SHORTCUT: {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shortcuts, parent, false);
                return new ShortcutViewHolder(itemView);
            }
            case TYPE_RECENT: {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery, parent, false);
                return new GalleryViewHolder(itemView);
            }
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shortcuts, parent, false);
        return new ShortcutViewHolder(itemView);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(ViewHolder item, View view, int position);

        void onItemLongClick(ViewHolder item, View view, int position);

        void onItemViewClick(ViewHolder item, View view, int position);
    }

    public abstract class ViewHolder extends RecyclerView.ViewHolder {
        protected final CircleImage iconBackground;
        protected final ImageView icon;
        ImageView ivRightButton;
        protected final TextView title;
        TextView tvMemorySize;
        protected TextView summary;
        protected NumberProgressBar progress;
        protected CircularProgressIndicator circularProgress;
        protected ImageButton action;
        protected View action_layout;
        protected FrameLayout flShortCut;
        protected LinearLayout card_view;
        protected final ImageView iconMime;
        protected final ImageView iconThumb;
        protected final View iconMimeBackground;
        public CommonInfo commonInfo;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != onItemClickListener) {
                        onItemClickListener.onItemClick(ViewHolder.this, v, getLayoutPosition());
                    }
                }
            });

            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (null != onItemClickListener) {
                        onItemClickListener.onItemLongClick(ViewHolder.this, v, getLayoutPosition());
                    }
                    return false;
                }
            });

            icon = v.findViewById(android.R.id.icon);
            iconBackground = v.findViewById(R.id.icon_background);
            title = v.findViewById(android.R.id.title);
            flShortCut = v.findViewById(R.id.flShortCut);
            tvMemorySize = v.findViewById(R.id.tvMemorySize);
            ivRightButton = v.findViewById(R.id.ivRightButton);

            card_view = v.findViewById(R.id.card_view);
            summary = v.findViewById(android.R.id.summary);
            progress = v.findViewById(android.R.id.progress);
            circularProgress = v.findViewById(R.id.circularProgress);
            action_layout = v.findViewById(R.id.action_layout);
            action = v.findViewById(R.id.action);

            iconMime = v.findViewById(R.id.icon_mime);
            iconThumb = v.findViewById(R.id.icon_thumb);
            iconMimeBackground = v.findViewById(R.id.icon_mime_background);
        }

        public abstract void setData(int position);
    }

    public class MainViewHolder extends ViewHolder {
        private final int accentColor;
        private final int color;

        public MainViewHolder(View v) {
            super(v);
            ivRightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != onItemClickListener) {
                        onItemClickListener.onItemViewClick(MainViewHolder.this, ivRightButton, getLayoutPosition());
                    }
                }
            });
            accentColor = SettingsActivity.getAccentColor();
            color = SettingsActivity.getPrimaryColor();
        }

        @Override
        public void setData(int position) {
            commonInfo = getItem(position);
            icon.setImageDrawable(commonInfo.rootInfo.loadDrawerIcon(mContext));
            title.setText(commonInfo.rootInfo.title);
            int drawableId = -1;
            if (commonInfo.rootInfo.isAppProcess()) {
//                drawableId = R.drawable.ic_broom;
                drawableId = R.drawable.ic_arrow_right_circle;
            } else if (commonInfo.rootInfo.isStorage() && !commonInfo.rootInfo.isSecondaryStorage()) {
                drawableId = R.drawable.ic_arrow_right_circle;
            } else {
                drawableId = R.drawable.ic_arrow_right_circle;
            }
            if (drawableId != -1) {
                ivRightButton.setImageDrawable(mContext.getResources().getDrawable(drawableId));
                action_layout.setVisibility(View.VISIBLE);
            } else {
                ivRightButton.setImageDrawable(null);
                action_layout.setVisibility(View.GONE);
            }

            if (position == 0) {
                card_view.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.accent_gradient));
            } else if (position == 1) {
                card_view.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.accent_gradient_reverse));
            } else if (position == 2) {
                isSDCard = true;
                card_view.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.accent_gradient));
            }

            if (commonInfo.rootInfo.availableBytes >= 0) {
                try {
                    Long current = 100 * commonInfo.rootInfo.availableBytes / commonInfo.rootInfo.totalBytes;
                    progress.setVisibility(View.VISIBLE);
                    progress.setMax(100);
                    progress.setProgress(100 - current.intValue());
                    progress.setColor(color);
                    animateProgress(progress, commonInfo.rootInfo);
                } catch (Exception e) {
                    progress.setVisibility(View.GONE);
                }

                try {
                    Long current = 100 * commonInfo.rootInfo.availableBytes / commonInfo.rootInfo.totalBytes;
                    circularProgress.setVisibility(View.VISIBLE);
                    circularProgress.setMaxProgress(100);
                    circularProgress.setProgress(100 - current.intValue(), 100);
                    circularProgress.setProgressColor(color);
                    circularProgress.setProgressTextAdapter(progressTextAdapter);
                    tvMemorySize.setText(getKbToMbOrGb(commonInfo.rootInfo.availableBytes) + " / " + getKbToMbOrGb(commonInfo.rootInfo.totalBytes));
                    animateProgress(progress, commonInfo.rootInfo);
                } catch (Exception e) {
                    progress.setVisibility(View.GONE);
                }
            } else {
                progress.setVisibility(View.GONE);
            }
        }
    }

    String getKbToMbOrGb(long size) {
        String hrSize = "";

        double b = size;
        double k = size / 1024.0;
        double m = ((size / 1024.0) / 1024.0);
        double g = (((size / 1024.0) / 1024.0) / 1024.0);
        double t = ((((size / 1024.0) / 1024.0) / 1024.0) / 1024.0);

        DecimalFormat dec = new DecimalFormat("0.00");

        if (t > 1) {
            hrSize = dec.format(t).concat(" TB");
        } else if (g > 1) {
            hrSize = dec.format(g).concat(" GB");
        } else if (m > 1) {
            hrSize = dec.format(m).concat(" MB");
        } else if (k > 1) {
            hrSize = dec.format(k).concat(" KB");
        } else {
            hrSize = dec.format(b).concat(" Bytes");
        }
        return hrSize;
    }

    private static final CircularProgressIndicator.ProgressTextAdapter progressTextAdapter = new CircularProgressIndicator.ProgressTextAdapter() {
        @Override
        public String formatText(double time) {
            int hours = (int) (time / 3600);
            time %= 3600;
            int minutes = (int) (time / 60);
            int seconds = (int) (time % 60);
            StringBuilder sb = new StringBuilder();
            if (hours < 10) {
                sb.append(0);
            }
            sb.append(hours).append(":");
            if (minutes < 10) {
                sb.append(0);
            }
            sb.append(minutes).append(":");
            if (seconds < 10) {
                sb.append(0);
            }
            sb.append(seconds);
//            return sb.toString();

            String progressPercentage = String.valueOf((int) time);
            return progressPercentage + "%";
        }
    };

    public class ShortcutViewHolder extends ViewHolder {

        public ShortcutViewHolder(View v) {
            super(v);
        }

        @Override
        public void setData(int position) {
            flShortCut.getLayoutParams().width = (Utils.getDeviceWidth(mContext) / 3);
            commonInfo = getItem(position);
            if (null == commonInfo || null == commonInfo.rootInfo) {
                return;
            }
//            iconBackground.setColor(ContextCompat.getColor(mContext, commonInfo.rootInfo.derivedColor));
            title.setText(commonInfo.rootInfo.title);

            if (isSDCard) {
                //VAAPGLKNS
                switch (position) {
                    case 3:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_connect_to_pc_new));
                        break;

                    case 4:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_apk_new));
                        break;

                    case 5:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_image_new));
                        break;

                    case 6:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_video_new));
                        break;

                    case 7:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_music_new));
                        break;

                    case 8:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_cleaner_new));
                        break;
                }
            } else {
                //VAAPGLKNS
                switch (position) {
                    case 2:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_connect_to_pc_new));
                        break;

                    case 3:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_apk_new));
                        break;

                    case 4:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_image_new));
                        break;

                    case 5:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_video_new));
                        break;

                    case 6:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_music_new));
                        break;

                    case 7:
                        icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_root_cleaner_new));
                        break;
                }
            }
//            icon.setImageDrawable(commonInfo.rootInfo.loadShortcutIcon(mContext));
        }
    }

    public class GalleryViewHolder extends ViewHolder {
        private final RecyclerViewPlus recyclerview;
        private TextView recents;
        private RecentsAdapter adapter;

        public GalleryViewHolder(View v) {
            super(v);

            recyclerview = v.findViewById(R.id.recyclerview);
            recents = v.findViewById(R.id.recents);
            recents.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != onItemClickListener) {
                        onItemClickListener.onItemViewClick(GalleryViewHolder.this, recents, getLayoutPosition());
                    }
                }
            });
        }

        @Override
        public void setData(int position) {
            commonInfo = CommonInfo.from(recentCursor);
            adapter = new RecentsAdapter(mContext, recentCursor, mIconHelper);
            adapter.setOnItemClickListener(new RecentsAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(RecentsAdapter.ViewHolder item, int position) {
                    if (null != onItemClickListener) {
                        onItemClickListener.onItemClick(GalleryViewHolder.this, recyclerview, position);
                    }
                }
            });
            recyclerview.setAdapter(adapter);
        }

        public DocumentInfo getItem(int position) {
            return DocumentInfo.fromDirectoryCursor(adapter.getItem(position));
        }
    }

    public CommonInfo getItem(int position) {
        if (position < mData.size()) {
            return mData.get(position);
        } else {
            return CommonInfo.from(recentCursor);
        }
    }

    private void animateProgress(final NumberProgressBar item, RootInfo root) {
        try {
            final double percent = (((root.totalBytes - root.availableBytes) / (double) root.totalBytes) * 100);
            final Timer timer = new Timer();
            item.setProgress(0);
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (Utils.isActivityAlive(mContext)) {
                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (item.getProgress() >= (int) percent) {
                                    timer.cancel();
                                } else {
                                    item.setProgress(item.getProgress() + 1);
                                }
                            }
                        });
                    }
                }
            }, 50, 20);
        } catch (Exception e) {
            item.setVisibility(View.GONE);
            CrashReportingManager.logException(e);
        }
    }
}