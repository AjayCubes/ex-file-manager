/*
 * Copyright (C) 2014 Hari Krishna Dulipudi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package file.manager.explorer.pro;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ShareCompat;

import com.hsalf.smilerating.SmileRating;

import file.manager.explorer.pro.misc.AnalyticsManager;
import file.manager.explorer.pro.misc.ColorUtils;
import file.manager.explorer.pro.misc.SystemBarTintManager;
import file.manager.explorer.pro.misc.Utils;
import file.manager.explorer.pro.setting.SettingsActivity;

import static file.manager.explorer.pro.DocumentsActivity.getStatusBarHeight;
import static file.manager.explorer.pro.misc.Utils.getSuffix;
import static file.manager.explorer.pro.misc.Utils.openFeedback;

public class AboutActivity extends AboutVariantFlavour implements View.OnClickListener {
    public static final String TAG = "About";

    //FOR OFFER
    private AlertDialog alertDialogOffer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Utils.hasKitKat() && !Utils.hasLollipop()) {
            setTheme(R.style.DocumentsTheme_Translucent);
        }
        setContentView(R.layout.activity_about);

        int color = SettingsActivity.getPrimaryColor();
        View view = findViewById(R.id.toolbar);
        if (view instanceof Toolbar) {
            Toolbar mToolbar = (Toolbar) view;
            mToolbar.setTitleTextAppearance(this, R.style.TextAppearance_AppCompat_Widget_ActionBar_Title);
            if (Utils.hasKitKat() && !Utils.hasLollipop()) {
                mToolbar.setPadding(0, getStatusBarHeight(this), 0, 0);
            }
            mToolbar.setBackgroundColor(color);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(null);
            setUpDefaultStatusBar();
        } else {
            view.setBackgroundColor(color);
        }
        initAd();
        initControls();
    }

    @Override
    public String getTag() {
        return TAG;
    }

    private void initControls() {

        int accentColor = ColorUtils.getTextColorForBackground(SettingsActivity.getPrimaryColor());
        TextView logo = (TextView) findViewById(R.id.logo);
        logo.setTextColor(accentColor);
        String header = logo.getText() + getSuffix() + " v" + file.manager.explorer.pro.BuildConfig.VERSION_NAME + (file.manager.explorer.pro.BuildConfig.DEBUG ? " Debug" : "");
        logo.setText(header);

        TextView action_rate = (TextView) findViewById(R.id.action_rate);
        TextView action_support = (TextView) findViewById(R.id.action_support);
        TextView action_share = (TextView) findViewById(R.id.action_share);
        TextView action_feedback = (TextView) findViewById(R.id.action_feedback);
        TextView action_sponsor = (TextView) findViewById(R.id.action_sponsor);

        action_rate.setOnClickListener(this);
        action_support.setOnClickListener(this);
        action_share.setOnClickListener(this);
        action_feedback.setOnClickListener(this);
        action_sponsor.setOnClickListener(this);

        if (Utils.isOtherBuild()) {
            action_rate.setVisibility(View.GONE);
            action_support.setVisibility(View.GONE);
        } else if (DocumentsApplication.isTelevision()) {
            action_share.setVisibility(View.GONE);
            action_feedback.setVisibility(View.GONE);
        }

        if (!DocumentsApplication.isPurchased()) {
            action_sponsor.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void startActivity(Intent intent) {
        if (Utils.isIntentAvailable(this, intent)) {
            super.startActivity(intent);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.action_feedback:
                openFeedback(this);
                break;

            case R.id.action_rate:
//				openPlaystore(this);
                showAd();
                showRateNowAlertDialog();
                AnalyticsManager.logEvent("app_rate");
                break;

            case R.id.action_sponsor:
                showAd();
                AnalyticsManager.logEvent("app_sponsor");
                break;

            case R.id.action_support:
                if (Utils.isProVersion()) {
                    Intent intentMarketAll = new Intent("android.intent.action.VIEW");
                    intentMarketAll.setData(Utils.getAppProStoreUri());
                    startActivity(intentMarketAll);
                } else {
                    DocumentsApplication.openPurchaseActivity(this);
                }
                AnalyticsManager.logEvent("app_love");
                break;

            case R.id.action_share:
                String shareText = "Hey guys, Try out this amazing File manager app!. " + Utils.getAppShareUri().toString();
                ShareCompat.IntentBuilder
                        .from(this)
                        .setText(shareText)
                        .setType("text/plain")
                        .setChooserTitle("Share EU file manager")
                        .startChooser();
                AnalyticsManager.logEvent("app_share");
                break;
        }
    }

    //region FOR SHOW RATE NOW DIALOG
    private void showRateNowAlertDialog() {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.row_offer_dialog, null);
        //INITIALIZE WIDGET
        final TextView tvTitle = alertLayout.findViewById(R.id.tvTitle);
        final TextView tvDescription = alertLayout.findViewById(R.id.tvDescription);
        final TextView tvSuggetion = alertLayout.findViewById(R.id.tvSuggestion);
        final ImageView ivLater = alertLayout.findViewById(R.id.ivLater);
        final ImageView ivRateNow = alertLayout.findViewById(R.id.ivRateNow);
        final ImageView ivClose = alertLayout.findViewById(R.id.ivClose);
        final ImageView ivRatingEmotion = alertLayout.findViewById(R.id.ivRatingEmotion);

        final RatingBar ratingBar = alertLayout.findViewById(R.id.ratingBar);
//        final SmileRating smileRating = alertLayout.findViewById(R.id.smileRating);
        /*LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);*/

//        tvTitle.setTypeface(Typefaces.regularFont(getApplicationContext()));
//        tvDescription.setTypeface(Typefaces.regularFont(getApplicationContext()));

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                switch ((int) v) {
                    case 0:
                        tvSuggetion.setTextColor(getResources().getColor(R.color.colorGray));
                        tvSuggetion.setText(getResources().getString(R.string.apply_rating));
                        ivRatingEmotion.setImageDrawable(getResources().getDrawable(R.drawable.sad));
                        break;
                    case 1:
                        tvSuggetion.setTextColor(getResources().getColor(R.color.colorGray));
                        tvSuggetion.setText(R.string.dont_like);
                        ivRatingEmotion.setImageDrawable(getResources().getDrawable(R.drawable.sad));
                        break;

                    case 2:
                        tvSuggetion.setTextColor(getResources().getColor(R.color.colorGray));
                        tvSuggetion.setText(R.string.having_issue);
                        ivRatingEmotion.setImageDrawable(getResources().getDrawable(R.drawable.sad));
                        break;

                    case 3:
                        tvSuggetion.setTextColor(getResources().getColor(R.color.colorGray));
                        tvSuggetion.setText(R.string.suggestion);
                        ivRatingEmotion.setImageDrawable(getResources().getDrawable(R.drawable.sad));
                        break;

                    case 4:
                        tvSuggetion.setTextColor(getResources().getColor(R.color.colorGray));
                        tvSuggetion.setText(R.string.its_okay);
                        ivRatingEmotion.setImageDrawable(getResources().getDrawable(R.drawable.wow));
                        break;

                    case 5:
                        tvSuggetion.setTextColor(getResources().getColor(R.color.colorGray));
                        tvSuggetion.setText(R.string.i_loved_it);
                        ivRatingEmotion.setImageDrawable(getResources().getDrawable(R.drawable.rate_liking));
                        break;
                }
            }
        });

        //FOR WIDGET OPERATIONS
        ivLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SAVE RATE RECORD IN SHARED PREFERENCES
//                SharedPreference.CreatePreferences(AboutActivity.this, Constant.fPREFERENCES);
//                SharedPreference.SavePreferences(Constant.IS_RATED, "0");
//                finish();

                alertDialogOffer.dismiss();
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SAVE RATE RECORD IN SHARED PREFERENCES...
//                SharedPreference.CreatePreferences(AboutActivity.this, Constant.fPREFERENCES);
//                SharedPreference.SavePreferences(Constant.IS_RATED, "0");
//                finish();

                alertDialogOffer.dismiss();
            }
        });

        ivRateNow.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                switch ((int) ratingBar.getRating()) {
//                switch (smileRating.getRating()) {
                    case 0:
                        tvSuggetion.setTextColor(getResources().getColor(R.color.red_color_picker));
                        tvSuggetion.setText(R.string.apply_rating);
                        break;
                    case 1:
                        /*rateGalleryViaMail();*/
                        Utils.shortToast(AboutActivity.this, getString(R.string.thanks_for_rating));
                        if (alertDialogOffer.isShowing()) {
                            alertDialogOffer.dismiss();
                        }
                        break;

                    case 2:
                        /*rateGalleryViaMail();*/
                        Utils.shortToast(AboutActivity.this, getString(R.string.thanks_for_rating));
                        if (alertDialogOffer.isShowing()) {
                            alertDialogOffer.dismiss();
                        }
                        break;

                    case 3:
                        /*rateGalleryViaMail();*/
                        Utils.shortToast(AboutActivity.this, getString(R.string.thanks_for_rating));
                        if (alertDialogOffer.isShowing()) {
                            alertDialogOffer.dismiss();
                        }
                        break;

                    case 4:
                        /*rateUs();*/
                        Utils.shortToast(AboutActivity.this, getString(R.string.thanks_for_rating));
                        if (alertDialogOffer.isShowing()) {
                            alertDialogOffer.dismiss();
                        }
                        break;

                    case 5:
                        rateUs();
                        break;
                }

                if (ratingBar.getRating() != 0) {
                    //SAVE RATE RECORD IN SHARED PREFERENCES...
                    SharedPreference.CreatePreferences(AboutActivity.this, Constant.fPREFERENCES);
                    SharedPreference.SavePreferences(Constant.IS_RATED, "1");
                }
            }
        });

        //FOR SET ALERT-DIALOG
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        alertDialogOffer = alert.create();
        alertDialogOffer.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.color_transparent)));
        alertDialogOffer.getWindow().getAttributes().windowAnimations = R.style.FadIn_FadOutAnimation;
        if (!alertDialogOffer.isShowing()) {
            alertDialogOffer.show();
        }

        /*AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Rate this App");
        alertDialog.setMessage("If you enjoy using Gallery, please take a moment to rate it. Thanks for your support..!");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Rate Gallery", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                rateUs();

                //SAVE RATE RECORD IN SHARED PREFERENCES...
                SharedPreference.CreatePreferences(MainActivity.this, Defaults.fPREFERENCES);
                SharedPreference.SavePreferences(Defaults.IS_RATED, "1");
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Remind me later", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

                //SAVE RATE RECORD IN SHARED PREFERENCES...
                SharedPreference.CreatePreferences(MainActivity.this, Defaults.fPREFERENCES);
                SharedPreference.SavePreferences(Defaults.IS_RATED, "0");
                finish();
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "No, thanks", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                //SAVE RATE RECORD IN SHARED PREFERENCES...
                SharedPreference.CreatePreferences(MainActivity.this, Defaults.fPREFERENCES);
                SharedPreference.SavePreferences(Defaults.IS_RATED, "1");
                finish();
            }
        });
        alertDialog.show();*/
    }
    //endregion

    //region FOR RATE US
    private void rateUs() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Unable to find market app..!", Toast.LENGTH_LONG).show();
        }

        if (alertDialogOffer != null && alertDialogOffer.isShowing())
            alertDialogOffer.dismiss();
    }
    //endregion


    public void setUpDefaultStatusBar() {
        int color = Utils.getStatusBarColor(SettingsActivity.getPrimaryColor());
        if (Utils.hasLollipop()) {
            getWindow().setStatusBarColor(color);
        } else if (Utils.hasKitKat()) {
            SystemBarTintManager systemBarTintManager = new SystemBarTintManager(this);
            systemBarTintManager.setTintColor(Utils.getStatusBarColor(color));
            systemBarTintManager.setStatusBarTintEnabled(true);
        }
    }
}